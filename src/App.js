import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <div className="App-header">
        <div className="main">
          <p>Ателье свадебного салона</p>
          <p>✆ +7 999 666 1212</p>
          <p>ПН-ПТ:09:00-20:00 </p>
          <p>СБ-ВС:10:00-18:00</p>
          <a href="#" className="navbar-brad"><img src="logo.png"></img></a> <hr/>
        </div>
        <div className='kn'>
          <a href="#" className="nav-link">Свадебные платья</a>
          <a href="#" className="nav-link">Распродажа</a>
          <a href="#" className="nav-link">Новости и акции</a>
          <a href="#" className="nav-link">Контакты</a>
          <a href="#" className="nav-link">О нас</a>
        </div>
        <div className="adaptivSlayder">
          <input type="radio" name="kadoves" id="slaid1" defaultChecked></input>
          <input type="radio" name="kadoves" id="slaid2"></input>
          <input type="radio" name="kadoves" id="slaid3"></input>
          <div className="kadoves">
            <label htmlFor="slaid1"></label>
            <label htmlFor="slaid2"></label>
            <label htmlFor="slaid3"></label>
          </div>
          <div className="adaptivSlayderlasekun">
            <div className="abusteku-deagulus">
              <img src="img1.jpg"/>
              <img src="img2.jpg"/>
              <img src="img3.jpg"/>
            </div>
          </div>
        </div>

        <div className='buy-info'>
          <div style={{display: 'flex', justifyContent: 'center'}}>
            <img style={{width: 400}} src='11.jpg'></img>
            <div style={{width: 500}}> 
              <button style={{width: '100%'}}><h1>Как купить товары онлайн</h1></button>

              <div className='buy-info-text'>
                <p>Добро пожаловать в лучший интернет-магазин где вы</p> 
                <p>найдете самые стильные свадебные платья, бижутерию и</p> 
                <p></p> <br/>
              </div>
            </div>
            <img style={{width: 400}} src='12.jpg'></img>
          </div>
        </div>

        <div className="uslugi">
          <b>Наши услуги</b>
          <div className="img">
            <img src='kons.jpg'/>
            <img src='2.jpg'/>
            <img src='3.jpg'/>
            <img src='4.jpg'/>
            <img src='5.jpg'/>
            <img src='6.jpg'/>
            <img src='7.jpg'/>
            <img src='8.jpg'/>
            <img src='9.jpg'/>
            <img src='10.jpg'/>
          </div>
        </div>
        <div className='sv'>
          <div>
            <b>Свадебный интернет магазин</b> <br></br>
            <p>Мы успешно работаем онлайн по всей России, а наши консультанты всегда на связи.</p> 
            <p>Поэтому, если вы хотите недорого купить свадебное платье в интернет-магазине, обязательно посмотрите наш каталог.</p> 
            <p>Интернет-магазин Mademoiselle - это 300 современных свадебных платьев, более 850 видов украшений и бижутерии, 300 видов фаты, 70 будуарных платьев и</p>
            <p>халатов, нижнее бельё, подвязки и множество приятных мелочей.</p> 
          </div>
        </div>
        <div class="cont">
          <div><img src='платье.jpg'></img></div>
          <div className='card-content'>
            <p>Купить свадебное платье и украшения онлайн в интернет-магазине Mademoiselle проще, чем вы думаете!</p>
          </div>
        </div>
        <div class="zak">
          <b>Требуется консультация специалиста ? Заполните форму и мы вам перезвоним!</b> <br/>
          <input type="text" placeholder="Введите имя" /> <br/>
          <input type="text" placeholder="Телефон/email"/> <br/>
          <a href="#" class="zakaz">Свяжитесь со мной</a>
        </div>
      </div>
    </div>
  );
}

export default App;